#!/bin/bash
set -e

sbt assembly

native-image --static \
  -H:+ReportExceptionStackTraces \
  -H:+AddAllCharsets \
  --allow-incomplete-classpath \
  --no-fallback \
  --initialize-at-build-time \
  --enable-http \
  --verbose \
  -jar "./target/scala-3.1.0/server.jar" \
  server-exe
