lazy val root = project
  .in(file("."))
  .settings(
    name := "mosaico",
    version := "0.2.0",
    scalacOptions += "-Ykind-projector",
    scalaVersion := "3.1.0",
    assemblyJarName := "server.jar",
    libraryDependencies ++= Seq(
      "org.http4s"    %% "http4s-dsl"          % "0.23.7",
      "org.http4s"    %% "http4s-blaze-server" % "0.23.7",
      "org.http4s"    %% "http4s-circe"        % "0.23.7",
      "io.circe"      %% "circe-core"          % "0.15.0-M1",
      "io.circe"      %% "circe-generic"       % "0.15.0-M1",
      "io.circe"      %% "circe-parser"        % "0.15.0-M1",
      "ch.qos.logback" % "logback-classic"     % "1.2.10"
    ),
    libraryDependencies ++= Seq(
      "org.scalameta" %% "munit" % "0.7.29" % Test
    )
  )
