package mosaico
import mosaico.groupAdjacentBy

import munit._
class UtilTest extends FunSuite {

  test("groupAdjacentBy") {
    val grouped = Vector(1, 2, 4, 6, 3, 5, 4, 4, 1).groupAdjacentBy(_ % 2 == _ % 2)
    val expected = Vector(
      Vector(1),
      Vector(2, 4, 6),
      Vector(3, 5),
      Vector(4, 4),
      Vector(1)
    )
    assert(grouped == expected)
  }

}
