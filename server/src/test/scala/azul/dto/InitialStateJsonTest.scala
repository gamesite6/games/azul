package mosaico.dto

import mosaico.Seed
import mosaico.shared.*
import io.circe.parser.*
import io.circe.syntax.given
import munit.*
import cats.implicits.given

import scala.language.implicitConversions

class InitialStateJsonTest extends FunSuite {

  test("read req json") {

    val req = decode[InitialStateReq]("""
        {
          "players": [1,2,3],
          "settings": {
            "selectedPlayerCounts": [3,4]
          },
          "seed": 42
        }
        """)

    assertEquals(
      req,
      InitialStateReq(
        players = Set(PlayerId(1), PlayerId(2), PlayerId(3)),
        settings = Settings(
          selectedPlayerCounts = Set(3, 4)
        ),
        seed = Seed(42)
      ).asRight
    )
  }

  test("write res json") {
    val res = InitialStateRes(
      state = GameState(
        players = Vector(
          Player(
            id = PlayerId(1),
            score = Score(0),
            patterns = Patterns.empty,
            wall = Wall.empty,
            floor = Floor.empty
          ),
          Player(
            id = PlayerId(2),
            score = Score(0),
            patterns = Patterns.empty,
            wall = Wall.empty,
            floor = Floor.empty
          ),
          Player(
            id = PlayerId(3),
            score = Score(0),
            patterns = Patterns.empty,
            wall = Wall.empty,
            floor = Floor.empty
          )
        ),
        phase = Phase.Picking(
          player = PlayerId(1),
          factories = Vector.empty,
          center = Vector.empty,
          pickedCenter = None
        ),
        bag = Vector(1, 2, 3, 4, 5).map(Tile.Colored.apply)
      )
    )

    val json = res.asJson

    assert(json.isObject)

  }

}
