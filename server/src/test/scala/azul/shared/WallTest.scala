package mosaico.shared
import munit._

class WallTest extends FunSuite {

  test("single connected tile is 2 points") {
    val wall = Wall.empty
      .insert(2, 3, Tile.Colored(1))
      .insert(2, 4, Tile.Colored(3))

    val score = wall.score(2, 3)
    assert {
      score == Score(2)
    }
  }

  test("unconnected tiles in same row/column do not count") {
    val wall = Wall.empty
      .insert(2, 2, Tile.Colored(1))
      .insert(2, 4, Tile.Colored(2))
      .insert(2, 0, Tile.Colored(3))
      .insert(4, 2, Tile.Colored(4))
      .insert(0, 2, Tile.Colored(5))

    val score = wall.score(2, 2)
    assert {
      score == Score(1)
    }
  }

  test("max possible score") {
    val wall = Wall.empty
      .insert(2, 0, Tile.Colored(1))
      .insert(2, 1, Tile.Colored(2))
      .insert(2, 2, Tile.Colored(3))
      .insert(2, 3, Tile.Colored(4))
      .insert(2, 4, Tile.Colored(5))
      .insert(0, 2, Tile.Colored(6))
      .insert(1, 2, Tile.Colored(7))
      .insert(3, 2, Tile.Colored(8))
      .insert(4, 2, Tile.Colored(9))

    val score = wall.score(2, 2)
    assert {
      score == Score(10)
    }
  }
}
