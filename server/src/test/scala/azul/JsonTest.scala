package mosaico

import mosaico.shared.{Action, Color, Floor, Patterns, Phase, Player, PlayerId, Score, Tile, Wall}
import io.circe.Decoder
import io.circe.syntax.given
import io.circe.parser.parse
import cats.implicits.given

class JsonTest extends munit.FunSuite {

  test("starting player marker") {
    val value = Tile.StartingPlayerMarker
    val encoded = value.asJson
    assertEquals(encoded, parse(""" "s" """).toOption.get)

    val decoded = encoded.as[Tile]
    assertEquals(decoded, value.asRight)
  }

  test("colored tile") {
    val value = Tile.Colored(12)
    val encoded = value.asJson
    assertEquals(encoded, parse("12").toOption.get)

    val decoded = encoded.as[Tile]
    assertEquals(decoded, value.asRight)
  }

  test("patterns") {
    val value = Patterns.empty
    val encoded = value.asJson
    assertEquals(encoded, parse("[[], [], [], [], []]").toOption.get)

    val decoded = encoded.as[Patterns]
    assertEquals(decoded, value.asRight)
  }

  test("wall") {

    val value = Wall.empty
    val encoded = value.asJson
    assertEquals(
      encoded,
      parse("""
      [
        ["b", "y", "r", "k", "w"],
        ["w", "b", "y", "r", "k"],
        ["k", "w", "b", "y", "r"],
        ["r", "k", "w", "b", "y"],
        ["y", "r", "k", "w", "b"]
      ]
      """).toOption.get
    )

    val decoded = encoded.as[Wall]
    assertEquals(decoded, value.asRight)
  }

  test("floor") {
    val value = Floor.empty
    val encoded = value.asJson
    assertEquals(encoded, parse("[]").toOption.get)

    val decoded = encoded.as[Floor]
    assertEquals(decoded, value.asRight)
  }

  test("player") {
    val value = Player(
      id = PlayerId(1),
      score = Score(42),
      patterns = Patterns.empty,
      wall = Wall.empty,
      floor = Floor.empty
    )
    val encoded = value.asJson
    assertEquals(
      encoded,
      parse("""
        |{
        | "id": 1,
        | "score": 42,
        | "patterns": [[], [], [], [], []],
        | "wall": [
        |   ["b", "y", "r", "k", "w"],
        |   ["w", "b", "y", "r", "k"],
        |   ["k", "w", "b", "y", "r"],
        |   ["r", "k", "w", "b", "y"],
        |   ["y", "r", "k", "w", "b"]
        | ],
        | "floor": []
        |}
        |""".stripMargin).toOption.get
    )

    val decoded = encoded.as[Player]
    assertEquals(decoded, value.asRight)
  }

  test("picking phase") {

    val value = Phase.Picking(
      factories = Vector(
        Vector(Tile.Colored(5), Tile.Colored(6)),
        Vector(Tile.Colored(7), Tile.Colored(8), Tile.Colored(9))
      ),
      center = Vector(Tile.Colored(10), Tile.StartingPlayerMarker),
      player = PlayerId(1),
      pickedCenter = None
    )
    val encoded = value.asJson
    assertEquals(
      encoded,
      parse("""
          |[
          | "Picking",
          | {
          |   "factories": [[5, 6], [7, 8, 9]],
          |   "center": [10, "s"],
          |   "player": 1,
          |   "pickedCenter": null
          | }
          |]
          |""".stripMargin).toOption.get
    )

    val decoded = encoded.as[Phase]
    assertEquals(decoded, value.asRight)
  }

  test("pick action") {

    val value = Action.Pick(tile = Tile.Colored(30), row = 3)
    val encoded = value.asJson
    assertEquals(
      encoded,
      parse("""
              |[
              | "Pick",
              | {
              |   "tile": 30,
              |   "row": 3
              | }
              |]
              |""".stripMargin).toOption.get
    )

    val decoded = encoded.as[Action]
    assertEquals(decoded, value.asRight)
  }
}
