package mosaico.dto

import mosaico.Seed
import mosaico.shared.{PlayerId, Settings}
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

final case class InitialStateReq(
    players: Set[PlayerId],
    settings: Settings,
    seed: Seed
)

object InitialStateReq {
  given Decoder[InitialStateReq] = deriveDecoder
}
