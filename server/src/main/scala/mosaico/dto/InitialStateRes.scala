package mosaico.dto

import mosaico.shared.GameState
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

final case class InitialStateRes(
    state: GameState
)

object InitialStateRes {
  given Encoder[InitialStateRes] = deriveEncoder
}
