package mosaico.dto

import io.circe.Encoder
import io.circe.generic.semiauto.*

case class InfoRes(
    playerCounts: List[Int]
)

object InfoRes {
  given Encoder[InfoRes] = deriveEncoder
}
