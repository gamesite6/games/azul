package mosaico.dto

import mosaico.shared.GameState
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

final case class PerformActionRes(
    completed: Boolean,
    nextState: GameState
)

object PerformActionRes {
  given Encoder[PerformActionRes] = deriveEncoder
}
