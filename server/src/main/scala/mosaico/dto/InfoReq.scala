package mosaico.dto
import mosaico.shared.Settings
import io.circe.Decoder
import io.circe.generic.semiauto.*

case class InfoReq(
    settings: Settings
)

object InfoReq {
  given Decoder[InfoReq] = deriveDecoder
}
