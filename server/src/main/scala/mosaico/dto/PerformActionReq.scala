package mosaico.dto

import mosaico.shared.{Action, GameState, PlayerId, Settings}
import mosaico.Seed
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

final case class PerformActionReq(
    action: Action,
    performedBy: PlayerId,
    settings: Settings,
    seed: Seed,
    state: GameState
)

object PerformActionReq {
  given Decoder[PerformActionReq] = deriveDecoder
}
