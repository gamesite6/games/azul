package mosaico.shared

import io.circe.{Encoder, Decoder}

opaque type Score = Int

object Score {
  given Encoder[Score] = Encoder.encodeInt
  given Decoder[Score] = Decoder.decodeInt

  def apply(score: Int): Score = score

  extension (score: Score) {
    def unwrap: Int = score

    def +(rhs: Score): Score = score + rhs
    def -(rhs: Score): Score = scala.math.max(score - rhs, 0)
    def *(rhs: Int): Score = score * rhs
  }

}
