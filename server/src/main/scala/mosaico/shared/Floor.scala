package mosaico.shared

import io.circe.{Codec, Decoder, Encoder}

opaque type Floor = Vector[Tile]
object Floor {
  given Encoder[Floor] = Encoder.encodeVector(Encoder[Tile])
  given Decoder[Floor] = Decoder.decodeVector(Decoder[Tile])

  def apply(floor: Vector[Tile]): Floor = floor
  def empty: Floor = Vector.empty

  extension (floor: Floor) {
    def unwrap: Vector[Tile] = floor

    private def indexPenalty(idx: Int): Score =
      idx match
        case 0 | 1 => Score(1)
        case 2 | 3 | 4 => Score(2)
        case _ => Score(3)

    def penalty: Score = floor.indices.map(indexPenalty).foldLeft(Score(0))(_ + _)

    def appendedAll(tiles: Iterable[Tile]): Floor = floor.appendedAll(tiles)
    def prependedAll(tiles: Iterable[Tile]): Floor = floor.prependedAll(tiles)

    def coloredTiles: Vector[Tile.Colored] = floor.flatMap {
      case tile: Tile.Colored => Some(tile)
      case _ => None
    }

    def truncated: (Floor, Vector[Tile]) = floor.splitAt(7)
  }
}
