package mosaico.shared
import mosaico.groupAdjacentBy

final case class GameState(
    players: Vector[Player],
    phase: Phase,
    bag: Vector[Tile.Colored]
) {

  def tilesInPlay: Iterable[Tile.Colored] = players.flatMap(_.tiles) ++ phase.tiles
  def discardedTiles: Iterable[Tile.Colored] = allColoredTilesSet -- tilesInPlay -- bag

  def prepareNextRound(startingPlayer: PlayerId, rng: util.Random): GameState =
    val factoryCount = getFactoryCount(players.size)
    val (factoryTiles, nextBag) =
      val tilesRequired = factoryCount * tilesPerFactory
      if tilesRequired > bag.size then (bag ++ rng.shuffle(discardedTiles)).splitAt(tilesRequired)
      else bag.splitAt(tilesRequired)

    val factories = factoryTiles.grouped(factoryCount).toVector.transpose
    val phase = Phase.Picking(
      factories = factories,
      center = Vector(Tile.StartingPlayerMarker),
      player = startingPlayer,
      pickedCenter = None
    )

    this.copy(
      phase = phase,
      bag = nextBag
    )

  def isComplete: Boolean = phase match {
    case _: Phase.Complete => true
    case _ => false
  }

  lazy val rankings: Vector[Vector[Ranking]] =
    players
      .map(Ranking(_))
      .sortBy(_.sortValue)
      .reverse
      .groupAdjacentBy(_.sortValue == _.sortValue)
}

val tilesPerFactory = 4

def getFactoryCount(playerCount: Int): Int =
  if playerCount < 3 then 5
  else if playerCount == 3 then 7
  else 9
