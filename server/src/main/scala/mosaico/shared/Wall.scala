package mosaico.shared
import cats.implicits.given
import mosaico.groupAdjacentBy
import io.circe.{Codec, Decoder, Encoder}

opaque type Wall = Vector[Cell]

enum Cell {
  case Empty
  case EmptyColored(color: Color)
  case Full(tile: Tile.Colored)

  def tileOption: Option[Tile.Colored] = this match
    case Full(tile) => Some(tile)
    case Empty | EmptyColored(_) => None
}

object Cell {
  given Encoder[Cell] = Encoder.encodeJson.contramap {
    case Empty => Encoder.encodeNone(None)
    case EmptyColored(color) => Color.encodeColor(color)
    case Full(tile) => Tile.encodeColored(tile)
  }
  given Decoder[Cell] = Decoder.decodeNone
    .map[Cell](_ => Empty)
    .or(Color.decodeColor.map[Cell](EmptyColored.apply))
    .or(Tile.decodeColored.map[Cell](Full.apply))

}

object Wall {
  given Encoder[Wall] = Encoder
    .encodeVector(Encoder.encodeVector(Encoder[Cell]))
    .contramap[Vector[Cell]](_.grouped(5).toVector)

  given Decoder[Wall] = Decoder.decodeVector(Decoder.decodeVector(Decoder[Cell])).map(_.flatten)

  def apply(wall: Vector[Cell]): Wall = wall

  def empty: Wall =
    import Color._
    Vector(
      Blue,
      Yellow,
      Red,
      Black,
      White,
      White,
      Blue,
      Yellow,
      Red,
      Black,
      Black,
      White,
      Blue,
      Yellow,
      Red,
      Red,
      Black,
      White,
      Blue,
      Yellow,
      Yellow,
      Red,
      Black,
      White,
      Blue
    ).map(Cell.EmptyColored(_))

  extension (wall: Wall) {
    def unwrap: Vector[Cell] = wall

    def get(row: Int, column: Int): Option[Cell] =
      wall.lift(row * 5 + column)
    def getTile(row: Int, column: Int): Option[Tile.Colored] =
      get(row = row, column = column).flatMap(_.tileOption)

    def accepts(row: Int, column: Int, tile: Tile.Colored): Boolean =
      wall.get(row = row, column = column) match
        case None => false
        case Some(cell) =>
          lazy val otherRowIndices = Vector(0, 1, 2, 3, 4).filterNot(_ == row)
          lazy val otherColumnIndices = Vector(0, 1, 2, 3, 4).filterNot(_ == column)

          lazy val otherTilesInRow: Vector[Tile.Colored] =
            otherColumnIndices.flatMap(colIdx => wall.getTile(row = row, column = colIdx))
          lazy val otherTilesInColumn: Vector[Tile.Colored] =
            otherRowIndices.flatMap(rowIdx => wall.getTile(row = rowIdx, column = column))

          lazy val legalCell = cell match
            case Cell.EmptyColored(color) => color == tile.color
            case Cell.Empty => true
            case Cell.Full(_) => false

          legalCell &&
          otherTilesInRow.forall(_.color != tile.color) &&
          otherTilesInColumn.forall(_.color != tile.color)

    def insert(row: Int, column: Int, tile: Tile.Colored): Wall =
      wall.updated(row * 5 + column, Cell.Full(tile))

    def score(row: Int, column: Int): Score =
      (
        for
          tile <- getTile(row = row, column = column)

          connectedRowTiles <-
            val rowTiles = getRow(row)
            val rowGroups = rowTiles.groupAdjacentBy(_.isDefined == _.isDefined).map(_.flatten)
            rowGroups.find(_.contains(tile))

          connectedColumnTiles <-
            val columnTiles = getColumn(column)
            val columnGroups =
              columnTiles.groupAdjacentBy(_.isDefined == _.isDefined).map(_.flatten)
            columnGroups.find(_.contains(tile))

          connectionScores = Vector(connectedRowTiles.size, connectedColumnTiles.size).filter(_ > 1)
        yield
          if connectionScores.nonEmpty then Score(connectionScores.sum)
          else Score(1)
      ).getOrElse(Score(0))

    def getRow(rowIndex: Int): Vector[Option[Tile.Colored]] =
      Vector(0, 1, 2, 3, 4).map { columnIndex =>
        getTile(row = rowIndex, column = columnIndex)
      }

    def getRows: Vector[Vector[Option[Tile.Colored]]] =
      wall.grouped(5).map(_.map(_.tileOption)).toVector
    def getColumns: Vector[Vector[Option[Tile.Colored]]] = getRows.transpose

    def getColumn(columnIndex: Int): Vector[Option[Tile.Colored]] =
      Vector(0, 1, 2, 3, 4).map(rowIndex => getTile(row = rowIndex, column = columnIndex))

    def acceptsInRow(row: Int, tile: Tile.Colored): Boolean =
      val columns = Vector(0, 1, 2, 3, 4)
      columns.exists { column =>
        wall.accepts(row = row, column = column, tile)
      }

    def tiles: Seq[Tile.Colored] = wall.flatMap(_.tileOption)
  }
}
