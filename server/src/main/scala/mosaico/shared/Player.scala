package mosaico.shared

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

final case class Player(
    id: PlayerId,
    score: Score,
    patterns: Patterns,
    wall: Wall,
    floor: Floor
) {
  lazy val tiles: Seq[Tile.Colored] = patterns.tiles ++ wall.tiles ++ floor.coloredTiles

  lazy val hasCompletedWallRow: Boolean = wall.getRows.exists(_.forall(_.isDefined))

  lazy val completedRows: Int = wall.getRows.filter(_.forall(_.isDefined)).size
  lazy val bonus: Bonus =
    def completedColumns = wall.getColumns.filter(_.forall(_.isDefined)).size
    def completedColors =
      wall.tiles
        .groupBy(_.color)
        .filter { (color, tiles) =>
          tiles.length == 5
        }
        .keys
        .size

    Bonus(
      row = Score(2) * completedRows,
      column = Score(7) * completedColumns,
      color = Score(10) * completedColors
    )
}

object Player {

  given Codec[Player] = deriveCodec

  def apply(playerId: PlayerId): Player =
    Player(
      id = playerId,
      score = Score(0),
      patterns = Patterns.empty,
      wall = Wall.empty,
      floor = Floor.empty
    )
}

final case class Bonus(
    row: Score,
    column: Score,
    color: Score
) {
  lazy val total: Score = row + column + color
}
