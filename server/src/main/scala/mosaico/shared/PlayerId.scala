package mosaico.shared

import io.circe.{Decoder, Encoder}

opaque type PlayerId = Int
object PlayerId {
  given Encoder[PlayerId] = Encoder.encodeInt
  given Decoder[PlayerId] = Decoder.decodeInt

  def apply(playerId: Int): PlayerId = playerId

  extension (playerId: PlayerId) {
    def toInt: Int = playerId
  }

}
