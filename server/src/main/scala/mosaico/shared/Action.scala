package mosaico.shared

import io.circe.{Decoder, Encoder, Json}
import io.circe.generic.auto.given
import io.circe.syntax.given
import cats.implicits.given

enum Action {
  case Pick(tile: Tile.Colored, row: Int)
  case Place(row: Int, column: Int)
  case Sweep
}
object Action {
  given Encoder[Action] = Encoder.encodeJson.contramap {
    case p: Action.Pick => Json.arr("Pick".asJson, p.asJson)
    case p: Action.Place => Json.arr("Place".asJson, p.asJson)
    case p: Action.Sweep.type => Json.arr("Sweep".asJson, p.asJson)
  }
  given Decoder[Action] = Decoder.decodeJson.emap { json =>
    (for
      arr <- json.asArray.toRight("action must be array")
      tag <- arr.headOption.flatMap(_.asString).toRight("action must have tag")
      action <- tag match {
        case "Pick" =>
          arr
            .lift(1)
            .toRight("action must have body")
            .flatMap(_.as[Action.Pick].leftMap(_.getMessage))

        case "Place" =>
          arr
            .lift(1)
            .toRight("action must have body")
            .flatMap(_.as[Action.Place].leftMap(_.getMessage))

        case "Sweep" => Sweep.asRight
        case _ => Left("invalid action tag")
      }
    yield action)

  }
}
