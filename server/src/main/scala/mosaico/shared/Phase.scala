package mosaico.shared

import io.circe.Json.JArray
import io.circe.{Codec, Decoder, Encoder, Json}
import io.circe.generic.semiauto.deriveCodec
import io.circe.generic.auto.*
import io.circe.syntax.given
import cats.implicits.given

enum Phase {
  case Picking(
      factories: Vector[Vector[Tile.Colored]],
      center: Vector[Tile],

      /** The player currently picking tiles. */
      player: PlayerId,

      /** The player who first picked tiles from the center. */
      pickedCenter: Option[PlayerId]
  )
  case Tiling(
      ready: Set[PlayerId],

      /** The player who will act first next round */
      first: PlayerId
  )
  case Complete(
      rankings: Vector[Vector[Ranking]]
  )

  def tiles: Iterable[Tile.Colored] = this match
    case phase: Picking =>
      phase.factories.flatten ++
        phase.center.flatMap {
          case tile: Tile.Colored => Some(tile)
          case _ => None
        }
    case _: Tiling => Iterable.empty
    case _: Complete => Iterable.empty
}

object Phase {
  given Encoder[Phase] = Encoder.encodeJson.contramap {
    case p: Phase.Picking => Json.arr("Picking".asJson, p.asJson)
    case p: Phase.Tiling => Json.arr("Tiling".asJson, p.asJson)
    case p: Phase.Complete => Json.arr("Complete".asJson, p.asJson)
  }
  given Decoder[Phase] = Decoder.decodeJson.emap { json =>
    (for
      arr <- json.asArray.toRight("phase must be array")
      tag <- arr.headOption.flatMap(_.asString).toRight("phase must have tag")
      body <- arr.lift(1).toRight("phase must have body")
      phase <- tag match {
        case "Picking" => body.as[Phase.Picking].leftMap(_.getMessage)
        case "Tiling" => body.as[Phase.Tiling].leftMap(_.getMessage)
        case "Complete" => body.as[Phase.Complete].leftMap(_.getMessage)
        case _ => Left("invalid phase tag")
      }
    yield phase)

  }
}
