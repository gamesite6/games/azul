package mosaico.shared

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

final case class Ranking(
    player: PlayerId,

    /** Score before adding bonus */
    score: Score,
    bonus: Bonus,

    /** Number of completed rows in the player's wall */
    rows: Int
) {
  lazy val sortValue: Int = (score.unwrap + bonus.total.unwrap) * 10 + rows
}

object Ranking {
  def apply(player: Player): Ranking = Ranking(
    player = player.id,
    score = player.score,
    bonus = player.bonus,
    rows = player.completedRows
  )

  given Codec[Ranking] = deriveCodec
}
