package mosaico.shared

final case class Settings(
    selectedPlayerCounts: Set[Int]
)

val validPlayerCounts = Set(2, 3, 4)

def validatePlayerCounts(settings: Settings): Set[Int] =
  validPlayerCounts.intersect(settings.selectedPlayerCounts)
