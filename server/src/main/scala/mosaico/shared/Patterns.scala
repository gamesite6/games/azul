package mosaico.shared

import io.circe.generic.semiauto.{deriveCodec, deriveEncoder}
import io.circe.{Codec, Decoder, Encoder}

opaque type Patterns = Vector[Vector[Tile.Colored]]

object Patterns {
  given Encoder[Patterns] = Encoder.encodeVector(Encoder.encodeVector(Tile.encodeColored))
  given Decoder[Patterns] = Decoder.decodeVector(Decoder.decodeVector(Tile.decodeColored))

  def apply(patterns: Vector[Vector[Tile.Colored]]): Patterns = patterns

  def empty: Patterns = Vector.fill(5)(Vector.empty)

  extension (patterns: Patterns) {

    def unwrap: Vector[Vector[Tile.Colored]] = patterns
    def accepts(row: RowIndex, color: Color): Boolean =
      patterns.lift(row) match {
        case Some(pattern) => pattern.forall(_.color == color)
        case None => false
      }

    def insert(row: RowIndex, tiles: Vector[Tile.Colored]): (OverflowedTiles, Patterns) =
      patterns.lift(row) match
        case Some(pattern) =>
          val (nextPattern, overflowed) = (pattern ++ tiles).splitAt(row + 1)
          (overflowed, patterns.updated(row, nextPattern))
        case None => (tiles.toVector, patterns)

    def firstFullRow: Option[(Vector[Tile.Colored], RowIndex)] =
      patterns.zipWithIndex.find { (row, idx) => row.size > idx }

    def get(idx: RowIndex): Option[Vector[Tile.Colored]] = patterns.lift(idx)

    def clearRow(idx: RowIndex): (Vector[Tile.Colored], Patterns) = idx match {
      case 0 | 1 | 2 | 3 | 4 => (patterns(idx), patterns.updated(idx, Vector.empty))
      case _ => (Vector.empty, patterns)
    }

    def tiles: Seq[Tile.Colored] = patterns.flatten
  }

  type RowIndex = Int
  type OverflowedTiles = Vector[Tile.Colored]
}
