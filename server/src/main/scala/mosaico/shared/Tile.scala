package mosaico.shared

import io.circe.{Codec, Decoder, Encoder, Json}
import cats.implicits.given
import io.circe.Json.JString

enum Tile {
  case Colored(number: Int)
  case StartingPlayerMarker
}
object Tile {

  extension (tile: Colored) {
    def color: Color = tile.number / 20 match {
      case 0 => Color.Blue
      case 1 => Color.Yellow
      case 2 => Color.Red
      case 3 => Color.Black
      case _ => Color.White
    }
  }

  given encodeColored: Encoder[Colored] = Encoder[Int].contramap(_.number)
  given decodeColored: Decoder[Colored] = Decoder[Int].map(Colored.apply)

  given encodeStartingMarker: Encoder[StartingPlayerMarker.type] =
    Encoder[String].contramap(_ => "s")
  given decodeStartingMarker: Decoder[StartingPlayerMarker.type] = Decoder[String].map { case "s" =>
    StartingPlayerMarker
  }

  given Encoder[Tile] = Encoder[Json].contramap {
    case colored: Tile.Colored => encodeColored(colored)
    case marker: Tile.StartingPlayerMarker.type => encodeStartingMarker(marker)
  }
  given Decoder[Tile] = decodeColored.widen.or(decodeStartingMarker.widen)

}

val allColoredTiles: Vector[Tile.Colored] = Vector.range(0, 100).map(Tile.Colored.apply)
val allColoredTilesSet: Set[Tile.Colored] = allColoredTiles.toSet

enum Color {
  case Blue
  case Yellow
  case Red
  case Black
  case White
}

object Color {
  given decodeColor: Decoder[Color] = Decoder[String].emap {
    case "b" => Color.Blue.asRight
    case "y" => Color.Yellow.asRight
    case "r" => Color.Red.asRight
    case "k" => Color.Black.asRight
    case "w" => Color.White.asRight
    case str => s"invalid color: $str".asLeft
  }
  given encodeColor: Encoder[Color] = Encoder[String].contramap {
    case Color.Blue => "b"
    case Color.Yellow => "y"
    case Color.Red => "r"
    case Color.Black => "k"
    case Color.White => "w"
  }
}
