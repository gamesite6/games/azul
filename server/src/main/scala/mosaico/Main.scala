package mosaico

import mosaico.dto.*
import mosaico.shared.*
import cats.effect.*
import cats.implicits.*
import org.http4s.*
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.circe.*
import org.http4s.dsl.io.*
import org.http4s.headers.*
import org.http4s.implicits.*

import scala.util.Try

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    for
      port <- IO(sys.env("PORT").toInt)
      _ <- BlazeServerBuilder[IO]
        .bindHttp(port, "0.0.0.0")
        .withHttpApp(routes)
        .serve
        .compile
        .drain
    yield ExitCode.Success

  private val routes = HttpRoutes
    .of[IO] {
      case httpReq @ POST -> Root / "info" =>
        for
          req <- httpReq.as[InfoReq]
          playerCounts = mosaico.shared.validatePlayerCounts(req.settings)
          res <- Ok(InfoRes(playerCounts.toList.sorted))
        yield res

      case httpReq @ POST -> Root / "initial-state" =>
        for
          req <- httpReq.as[InitialStateReq]
          res <- initialState(req.players, req.settings, req.seed) match
            case Some(state) => Ok(InitialStateRes(state))
            case None => UnprocessableEntity()
        yield res

      case httpReq @ POST -> Root / "perform-action" =>
        for
          req <- httpReq.as[PerformActionReq]
          res <-
            performAction(
              previousState = req.state,
              settings = req.settings,
              action = req.action,
              userId = req.performedBy,
              seed = req.seed
            ) match {
              case Some(nextState) =>
                Ok(
                  PerformActionRes(completed = nextState.isComplete, nextState)
                )
              case None => UnprocessableEntity()
            }
        yield res

    }
    .orNotFound

  given EntityDecoder[IO, InfoReq] = jsonOf
  given EntityEncoder[IO, InfoRes] = jsonEncoderOf

  given EntityDecoder[IO, InitialStateReq] = jsonOf
  given EntityEncoder[IO, InitialStateRes] = jsonEncoderOf

  given EntityDecoder[IO, PerformActionReq] = jsonOf
  given EntityEncoder[IO, PerformActionRes] = jsonEncoderOf
}
