package mosaico

import mosaico.shared._
import cats.implicits._
import scala.language.implicitConversions

def initialState(
    playerIds: Set[PlayerId],
    settings: Settings,
    seed: Seed
): Option[GameState] =
  val rng = util.Random(seed.toLong)
  val players = rng.shuffle(playerIds.toVector).map(Player(_))

  for {
    factoryCount <- playerIds.size match {
      case 2 => 5.some
      case 3 => 7.some
      case 4 => 9.some
      case _ => None
    }
    tiles = rng.shuffle(allColoredTiles)
    (factoryTiles, bag) = tiles.splitAt(factoryCount * 4)
    factories = factoryTiles.grouped(4).toVector

  } yield GameState(
    players = players,
    phase = Phase.Picking(
      player = players.head.id,
      factories = factories,
      center = Vector[Tile](Tile.StartingPlayerMarker),
      pickedCenter = None
    ),
    bag = bag
  )
