package mosaico

import cats.implicits._
import scala.collection.immutable.VectorBuilder

extension [A](vec: Vector[A])
  def groupAdjacentBy(f: (A, A) => Boolean): Vector[Vector[A]] =
    vec.headOption match {
      case None => Vector.empty
      case Some(a) =>
        val result: VectorBuilder[Vector[A]] = new VectorBuilder
        var prev = a;
        var group: VectorBuilder[A] = new VectorBuilder
        group += a

        vec.tail.foreach { a =>
          if f(prev, a) then group += a
          else
            result += group.result
            group = new VectorBuilder
            group += a

          prev = a
        }

        result += group.result
        result.result
    }
