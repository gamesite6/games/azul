package mosaico

import mosaico.shared._
import cats.data.OptionT
import cats.data.State
import cats.implicits._

import ActionM._
import Tile._

type ActionM[A] = OptionT[State[GameState, *], A]

object ActionM {
  def get: ActionM[GameState] = OptionT.liftF(State.get[GameState])
  def set(s: GameState): ActionM[Unit] = OptionT.liftF(State.set(s))
  def modify(f: GameState => GameState): ActionM[Unit] =
    OptionT.liftF(State.modify(f))
  def modifyOption(f: GameState => Option[GameState]): ActionM[Unit] =
    get.map(f).flatMap {
      case Some(s) => set(s)
      case None => OptionT.fromOption(None)
    }
  def liftOption[A](a: Option[A]): ActionM[A] = OptionT.fromOption(a)
  def nothing[A]: ActionM[A] = liftOption(None)

  def modifyPlayer[A](
      playerId: PlayerId
  )(f: Player => (Player, A)): ActionM[A] =
    get.flatMap { gameState =>
      gameState.players.indexWhere(_.id == playerId) match {
        case -1 => nothing
        case idx =>
          val player = gameState.players(idx)
          val (updatedPlayer, a) = f(player)
          set(
            gameState.copy(players = gameState.players.updated(idx, updatedPlayer))
          ).map(_ => a)
      }
    }
  def modifyPlayerOption[A](
      playerId: PlayerId
  )(f: Player => Option[(Player, A)]): ActionM[A] =
    get.flatMap { gameState =>
      gameState.players.indexWhere(_.id == playerId) match {
        case -1 => nothing
        case idx =>
          val player = gameState.players(idx)
          f(player) match
            case Some((updatedPlayer, a)) =>
              set(
                gameState.copy(players = gameState.players.updated(idx, updatedPlayer))
              ).map(_ => a)
            case None => nothing
      }
    }
}

def performAction(
    previousState: GameState,
    settings: Settings,
    action: Action,
    userId: PlayerId,
    seed: Seed
): Option[GameState] =
  val rng = util.Random(seed.toLong)
  if actionIsAllowed(action, previousState, userId, settings) then
    val (_, nextState) = performActionM(settings, action, userId, rng).value
      .run(previousState)
      .value
    nextState
  else None

private def actionIsAllowed(
    action: Action,
    state: GameState,
    userId: PlayerId,
    settings: Settings
): Boolean =
  state.players.find(_.id == userId).fold(false) { user =>
    state.phase match
      case phase: Phase.Picking =>
        (phase.player == userId) &&
        (action match
          case action: Action.Pick =>
            val tileExists =
              phase.center.contains(action.tile) ||
                phase.factories.exists(_.contains(action.tile))

            val destinationIsLegal =
              action.row == 5 ||
                (user.wall.acceptsInRow(row = action.row, action.tile) &&
                  user.patterns.accepts(action.row, action.tile.color))

            tileExists && destinationIsLegal

          case _: Action.Place => false
          case Action.Sweep => false
        )
      case phase: Phase.Tiling =>
        !phase.ready.contains(userId) &&
        (action match
          case _: Action.Pick => false
          case action: Action.Place =>
            user.patterns.firstFullRow match
              case Some((fullRow, fullRowIdx)) =>
                fullRow.lastOption match {
                  case Some(tile) =>
                    fullRowIdx == action.row &&
                    user.wall.accepts(
                      row = action.row,
                      column = action.column,
                      tile = tile
                    )
                  case None => false
                }
              case None => false
          case Action.Sweep => user.patterns.firstFullRow.isEmpty
        )
      case _: Phase.Complete => false

  }

private def performActionM(
    settings: Settings,
    action: Action,
    userId: PlayerId,
    rng: util.Random
): ActionM[GameState] =
  for
    previousState <- get
    _ <- previousState.phase match
      case phase: Phase.Picking =>
        action match
          case action: Action.Pick =>
            if phase.center.contains(action.tile) then
              val (picked, remainingCenter) = phase.center.partition {
                case tile: Tile.Colored => tile.color == action.tile.color
                case Tile.StartingPlayerMarker => true
              }
              val (pickedMarker, pickedColoredTiles) =
                picked.partitionMap[Tile, Tile.Colored] {
                  case tile: Tile.Colored => Right(tile)
                  case Tile.StartingPlayerMarker =>
                    val marker: Tile = StartingPlayerMarker
                    Left(marker)
                }

              for
                _ <- modifyPlayer(userId) { user =>
                  val (overflowed, nextPatterns) =
                    user.patterns.insert(action.row, pickedColoredTiles)
                  (
                    user.copy(
                      patterns = nextPatterns,
                      floor = user.floor
                        .prependedAll(pickedMarker)
                        .appendedAll(overflowed)
                    ),
                    ()
                  )
                }
                _ <- discardOverflowingFloors
                _ <- modify { gameState =>
                  gameState.copy(
                    phase =
                      if phase.factories.forall(
                          _.isEmpty
                        ) && remainingCenter.isEmpty
                      then
                        Phase.Tiling(
                          ready = Set.empty,
                          first = phase.pickedCenter.getOrElse(userId)
                        )
                      else
                        phase.copy(
                          player = nextPlayer(gameState.players, phase.player),
                          center = remainingCenter,
                          pickedCenter = phase.pickedCenter.orElse(Some(userId))
                        ),
                  )
                }
              yield ()
            else
              val factory_idx =
                phase.factories.indexWhere(_.contains(action.tile))
              if factory_idx != -1 then
                val factory = phase.factories(factory_idx)
                val (picked, otherTiles) =
                  factory.partition(_.color == action.tile.color)
                for
                  _ <- modifyPlayer(userId) { user =>
                    val (overflowed, nextPatterns) =
                      user.patterns.insert(action.row, picked)
                    (
                      user.copy(
                        patterns = nextPatterns,
                        floor = user.floor.appendedAll(overflowed)
                      ),
                      ()
                    )
                  }
                  _ <-
                    modify { gameState =>
                      gameState.copy(
                        phase = {
                          val factories =
                            phase.factories.updated(factory_idx, Vector.empty)
                          val center: Vector[Tile] = phase.center ++ otherTiles

                          if factories.forall(_.isEmpty) && center.isEmpty then
                            Phase.Tiling(
                              ready = Set.empty,
                              first = phase.pickedCenter.getOrElse(userId)
                            )
                          else
                            phase.copy(
                              player = nextPlayer(previousState.players, phase.player),
                              center = center,
                              factories = factories
                            )
                        }
                      )
                    }
                yield ()
              else nothing
          case _: Action.Place => nothing
          case Action.Sweep => nothing

      case phase: Phase.Tiling =>
        action match
          case action: Action.Pick => nothing
          case action: Action.Place =>
            for
              _ <- modifyPlayerOption(userId) { user =>
                val (tiles, updatedPatterns) =
                  user.patterns.clearRow(action.row)

                for
                  tile <- tiles.lastOption
                  updatedWall = user.wall.insert(
                    row = action.row,
                    column = action.column,
                    tile = tile
                  )
                  addedScore = updatedWall.score(
                    row = action.row,
                    column = action.column
                  )
                yield (
                  user.copy(
                    score = user.score + addedScore,
                    patterns = updatedPatterns,
                    wall = updatedWall
                  ),
                  ()
                )
              }
            yield ()
          case Action.Sweep =>
            for
              _ <- modifyPlayer(userId) { user =>
                val penalty = user.floor.penalty
                val updatedUser =
                  user.copy(score = user.score - penalty, floor = Floor.empty)

                (updatedUser, ())
              }
              _ <- modify { gameState =>
                val ready = phase.ready + userId
                if gameState.players.forall(player => ready.contains(player.id)) then
                  if gameState.players.exists(_.hasCompletedWallRow) then
                    gameState.copy(
                      players = gameState.players.map(player =>
                        player.copy(
                          score = player.score + player.bonus.total
                        )
                      ),
                      phase = Phase.Complete(
                        rankings = gameState.rankings
                      )
                    )
                  else gameState.prepareNextRound(phase.first, rng)
                else
                  gameState.copy(
                    phase = phase.copy(ready = ready)
                  )
              }
            yield ()
      case _: Phase.Complete => nothing

    nextState <- get
  yield nextState

def nextPlayer(players: Vector[Player], currentPlayerId: PlayerId): PlayerId =
  val playerIdx = players.indexWhere(_.id == currentPlayerId)
  val nextPlayerIdx = (playerIdx + 1) % (players.size)
  players(nextPlayerIdx).id

def discardOverflowingFloors: ActionM[Unit] = modify { gameState =>
  val players = gameState.players.map { player =>
    val (floor: Floor, overflowedTiles: Vector[Tile]) = player.floor.truncated
    player.copy(floor = floor)
  }

  gameState.copy(
    players = players
  )
}
