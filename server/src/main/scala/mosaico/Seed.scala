package mosaico

import io.circe.{Encoder, Decoder}

opaque type Seed = Long

object Seed {
  def apply(seed: Long): Seed = seed

  extension (seed: Seed) {
    def toLong: Long = seed
  }

  given Encoder[Seed] = Encoder.encodeLong
  given Decoder[Seed] = Decoder.decodeLong
}
