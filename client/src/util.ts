import { createEventDispatcher } from "svelte";

export function createActionDispatcher(): (action: Action) => void {
  const dispatch = createEventDispatcher();
  return (action: Action) => {
    dispatch("action", action);
  };
}

export function isActive(state: GameState, playerId: PlayerId): boolean {
  switch (state.phase[0]) {
    case "Picking":
      return playerId == state.phase[1].player;
    case "Tiling":
      return !state.phase[1].ready.includes(playerId);
    case "Complete":
      return false;
  }
}

export function getCenter(state: GameState): Array<Tile> {
  switch (state.phase[0]) {
    case "Picking":
      return state.phase[1].center;
    case "Tiling":
    case "Complete":
      return [];
  }
}
export function getFactories(state: GameState): Array<Array<Tile>> {
  switch (state.phase[0]) {
    case "Picking":
      return state.phase[1].factories;
    case "Tiling":
    case "Complete":
      return [];
  }
}

type FactoryIndex = number;
type TileGroup = "center" | FactoryIndex;

export function getGroup(state: GameState, tile: Tile): TileGroup | null {
  if (getCenter(state).includes(tile)) {
    return "center";
  } else {
    const factoryIndex = getFactories(state).findIndex((factory) =>
      factory.includes(tile)
    );
    return factoryIndex !== -1 ? factoryIndex : null;
  }
}

export function withUserLast(
  players: Player[],
  userId: PlayerId | null
): Player[] {
  const userIdx = players.findIndex((p) => p.id === userId);
  if (userId === null || userIdx === -1) {
    return players;
  } else {
    return [...players.slice(userIdx + 1), ...players.slice(0, userIdx + 1)];
  }
}

export function isFilled(cell: Cell): cell is ColoredTile {
  return typeof cell === "number";
}

export function isEmptyCell(cell: Cell): cell is EmptyCell {
  return Array.isArray(cell) || typeof cell === "string";
}

export function isColoredCell(cell: Cell): cell is ColorJson {
  return typeof cell === "string";
}

export function getCellColor(cell: Cell): Color | null {
  return isColoredCell(cell) ? jsonToColor(cell) : null;
}

export function isColored(tile: Tile): tile is ColoredTile {
  return typeof tile === "number";
}

export enum Color {
  Blue = "blue",
  Yellow = "yellow",
  Red = "red",
  Black = "black",
  White = "white",
}

function jsonToColor(colorJson: ColorJson): Color {
  switch (colorJson) {
    case "b":
      return Color.Blue;
    case "y":
      return Color.Yellow;
    case "r":
      return Color.Red;
    case "k":
      return Color.Black;
    case "w":
      return Color.White;
  }
}

function colorToJson(color: Color): ColorJson {
  switch (color) {
    case Color.Blue:
      return "b";
    case Color.Yellow:
      return "y";
    case Color.Red:
      return "r";
    case Color.Black:
      return "k";
    case Color.White:
      return "w";
  }
}

export function getColor(tile: ColoredTile): Color {
  switch (Math.floor(tile / 20)) {
    case 0:
      return Color.Blue;
    case 1:
      return Color.Yellow;
    case 2:
      return Color.Red;
    case 3:
      return Color.Black;
    default:
      return Color.White;
  }
}

export function canPickTile(
  player: Player,
  tile: ColoredTile,
  patternRowIdx: number
): boolean {
  const row = player.patterns[patternRowIdx];

  if (patternRowIdx === 5) {
    // place it on the floor
    return true;
  }
  if (row === undefined) {
    return false;
  } else if (row.length === 0) {
    return true;
  } else if (row.length === patternRowIdx + 1) {
    return false;
  } else {
    const previousTile = row[0];
    return getColor(previousTile) === getColor(tile);
  }
}

export function isDonePlacingTiles(player: Player): boolean {
  return player.patterns.every((pattern, idx) => pattern.length < idx + 1);
}

export function getFloor(player: Player): Array<Tile | null> {
  const nulls: Array<null> = Array(7).fill(null);
  const floor = [...player.floor, ...nulls];
  return floor.slice(0, 7);
}

export function getWinners(players: Player[]): Player[] {
  return [];
}

export function getTileEmoji(color: Color): string {
  switch (color) {
    case Color.Blue:
      return "💧";
    case Color.Red:
      return "🍁";
    case Color.Yellow:
      return "☀️";
    case Color.Black:
      return "🌑";
    case Color.White:
      return "❄️";
  }
}
