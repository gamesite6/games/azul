import { crossfade, fly } from "svelte/transition";

export let [send, receive] = crossfade({
  fallback: (element, params, intro) => {
    return fly(element, { duration: 500, y: -25 });
  },
  duration: (d) => Math.sqrt(d * 500),
});
