type PlayerId = number;
type GameState = {
  players: Player[];
  phase: Phase;
};

type Option<A> = [A] | [];

type EmptyCell = [] | ColorJson;
type Cell = EmptyCell | ColoredTile;

type Player = {
  id: PlayerId;
  score: number;
  patterns: Array<Array<ColoredTile>>;
  wall: Array<Array<Cell>>;
  floor: Array<Tile>;
};

type ColoredTile = number;
type StartingPlayerMarker = "s";
type Tile = ColoredTile | StartingPlayerMarker;

type Bonus = {
  row: number;
  column: number;
  color: number;
};

type Phase =
  | [
      "Picking",
      {
        factories: Array<Array<Tile>>;
        center: Tile[];
        player: PlayerId;
        pickedCenter: Option<PlayerId>;
      }
    ]
  | ["Tiling", { ready: Array<PlayerId>; first: PlayerId }]
  | ["Complete", Complete];

type Complete = { rankings: Array<Array<Ranking>> };

type Ranking = {
  player: PlayerId;
  score: number;
  bonus: {
    row: number;
    column: number;
    color: number;
  };
  rows: number;
};

type GameSettings = {
  selectedPlayerCounts: number[];
};

type ColorJson = "b" | "y" | "r" | "k" | "w";

type Action =
  | ["Pick", { tile: Tile; row: number }]
  | ["Place", { row: number; column: number }]
  | ["Sweep"];
